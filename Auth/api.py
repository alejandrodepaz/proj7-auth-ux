# Laptop Service

import flask, sys
from flask import jsonify, Flask, request, render_template, redirect, url_for, flash, Markup, session
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from flask_restful import Resource, Api, abort

import csv, json
import os
from pymongo import MongoClient
import logging

from wtforms import Form, BooleanField, StringField, PasswordField, validators

from flask_wtf import FlaskForm
from flask_wtf.csrf import CSRFProtect

from wtforms.validators import DataRequired, Length

from passlib.apps import custom_app_context as pwd_context

# Instantiate the app
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
userdb = client.userdb

from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, 
                            confirm_login, fresh_login_required)

from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time

from wtforms.csrf.core import CSRF

###
# Pages
###

login_manager = LoginManager()

login_manager.login_view = "index"
login_manager.login_message = "Please log in to access this page."
login_manager.refresh_view = "reauth"

USERS_DATA = []

start = None
finish = None

id_val = 0

def generate_auth_token(ide, expiration=600):
   s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
   # pass index of user
   return s.dumps({'id': ide})

def verify_auth_token(token):
    s = Serializer(app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    return "Success"

@login_manager.user_loader
def load_user(ide):
    
    for user in USERS_DATA:
        if str(user.ide) == ide:
            return user
    
    return None
        

login_manager.setup_app(app)

class User(UserMixin):
    def __init__(self, username, ide, password, active=True):
        self.username = username
        self.password = password
        self.ide = ide
        self.active = True

    def is_active(self):
        return self.active

    def get_id(self):
        return self.ide

def hash_password(password):
    return pwd_context.encrypt(password)

def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)

class RegistrationForm(FlaskForm):
    username = StringField("Username", validators = [DataRequired(), Length(min = 5, max = 35)])
    password = PasswordField("Password", validators=[DataRequired(), Length(min = 5, max = 35)])
    
class LoginForm(FlaskForm):
    username = StringField("Username", validators = [DataRequired(), Length(min = 5, max = 35)])
    password = PasswordField("Password", validators=[DataRequired(), Length(min = 5, max = 35)])
    remember_me = BooleanField('Remember Me')

@app.route("/api/token", methods = ["GET"])
def getToken():

    global finish, start

    if session["token"] != None:
        
        toke = session["token"]
        token = str(session["token"])
        if verify_auth_token(toke) == "Success":
            finish = int(time.time())
            time_stamp = 600 - (finish - start)
            result = jsonify(token = token, duration = str(time_stamp))
            return result
        else:
            return unauthorized(401)
    else:
        return unauthorized(401)
    

    
@app.route("/api/register", methods = ["POST", "GET"])
def register():
    
    global userdb, id_val, USERS_DATA
    
    USERS_DATA = []
    form = RegistrationForm()
    if form.validate_on_submit():

        # get username and password from string fields
        username = form.username.data
        password = form.password.data

        #hash the password
        hVal = hash_password(password)
        
        users = userdb.userdb.find()
        dict_values = [item for item in users]

        id_val = len(dict_values) + 1
        
        user_data = {"id":str(id_val), "username":username, "password": hVal}
        
        userdb.userdb.insert_one(user_data)
        users = userdb.userdb.find()
        dict_values = [item for item in users]
   
        for val in dict_values:
            user = User(val["username"],val["id"], val["password"])
            USERS_DATA.append(user)


        response = jsonify(username=username)
        response.status_code = 201
        response.headers['location'] = '/' + username + "/" + str(id_val)
            
        flash("Successfully registered, please login with your credentials.")
        return response
    

    #flash("Failed to Register, Please Try Again")
    return render_template("register.html", form = form)


@app.route("/login", methods=["GET", "POST"])
def login():

    global USERS_DATA, start
    
    form = LoginForm()
    if form.validate_on_submit():
        user_name = form.username.data
        pass_word = form.password.data
        remember = form.remember_me.data
        
        for user in USERS_DATA:
            if user.username == user_name and verify_password(pass_word, user.password):
            
                remember_bool = form.remember_me.data
                
                if login_user(user, remember=remember_bool):
                    session["token"] = generate_auth_token(user.ide)
                    start = int(time.time())
                    flash("Logged in!")
                    return redirect(url_for("index"))
            
                else:
                    flash("Sorry, but you could not log in.")
                
            
        flash("Invalid username or password.")
                
    return render_template("login.html", form = form)

    
@app.route("/logout")
@login_required
def logout():
    logout_user()
    session["token"] = None
    flash("Logged out.")
    return redirect(url_for("index"))

@app.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    if request.method == "POST":
        confirm_login()
        flash("Reauthenticated.")
        return redirect(request.args.get("next") or url_for("index"))
    return render_template("reauth.html")


@app.route("/")
@app.route("/index")
def index():
    
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.errorhandler(401)
def unauthorized(error):
    return flask.render_template('401.html'), 401

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():

    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('distance',type=str)
    brevet_start_date = request.args.get('begin_date', type=str)
    brevet_start_time = request.args.get('begin_time', type=str)
    brevet_start = brevet_start_date + " " + brevet_start_time

    if km > int(dist[:-2])*1.2:
        result = {"open": 0, "close": 0}
        return flask.jsonify(result = result)

    if km > int(dist[:-2]) and km <= int(dist[:-2])*1.2:
        km = int(dist[:-2])
    
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, int(dist[:-2]), brevet_start)
    close_time = acp_times.close_time(km, int(dist[:-2]), brevet_start)

    if km ==int(dist[:-2]):
        close = arrow.get(close_time)
        if km == 200:
            close = close.shift(minutes =+ 10)
            close_time = close.isoformat()
        elif km == 400:
            close = close.shift(minutes =+ 20)
            close_time = close.isoformat()

    result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)


#############
open_confirmed = []
close_confirmed = []
km_confirmed = []

_times = db.tododb.find()
times = [item for item in _times]

@app.route('/display', methods=["POST"])
def todo():
    global db, client, open_confirmed, close_confirmed, km_confirmed, _times, times

    _items = db.tododb.find()
    items = [item for item in _items]
    length = len(items)
    
    if length == 0:
        flash("Database is Empty, Please Submit Form with Values. Database Clears After Invocation of 'Display'.")
        return redirect(url_for("index"))

    db.tododb.drop()
    db = client.tododb

    _times = db.tododb.find()
    times = [item for item in _times]
    
    open_confirmed = []
    close_confirmed = []
    km_confirmed = []
    
    flash("Display Button Was Clicked")
    return flask.render_template('db_display.html', items=items)


@app.route('/new', methods = ["POST"])
def new():


    global open_confirmed, close_confirmed, km_confirmed, _times, times
    
    open_time = request.form.getlist("open")
    close_time = request.form.getlist("close")
    controls = request.form.getlist("km")

    open_times = []
    close_times = []
    km = []

    for i in open_time:
        i = str(i)
        if (i != "") and (i not in open_confirmed):
            open_times.append(i)

    for j in close_time:
        j = str(j)
        if (j != "") and (j not in close_confirmed):
            close_times.append(j)

    for l in controls:
        l = str(l)
        if (l != "") and (l not in km_confirmed):
            km.append(l)

    length = len(open_times)

    if len(open_times) == 0 or len(close_times) == 0:
        flash("No New Controls Were Given")
        return redirect(url_for("index"))
    
        
    for k in range(length):

        open_confirmed.append(open_times[k])
        close_confirmed.append(close_times[k])
        km_confirmed.append(km[k])

        times = {"open_time": open_times[k], "close_time": close_times[k], "km": km[k]}

        db.tododb.insert_one(times)

    flash("Submit Button Was Clicked!")
    
    _times = db.tododb.find()
    times = [item for item in _times]
            
    return redirect(url_for("index"))


class listAll1(Resource):
    
    def get(self):

        try:
            toke = session["token"]
            if verify_auth_token(toke) != "Success":
                abort(401, error_message = "You Are Not Authorized to Access this Page")
        except:
            abort(401, error_message = "You Are Not Authorized to Access this Page")
            
        
        global times

        vals = {}
        for i in times:
            vals[i["km"]] = ["open time: " + i["open_time"], "close time: " + i["close_time"]]
                
        return vals

        
class listOpenOnly1(Resource):

    def get(self):
        global times

        try:
            toke = session["token"]
            if verify_auth_token(toke) != "Success":
                abort(401, error_message = "You Are Not Authorized to Access this Page")
        except:
            abort(401, error_message = "You Are Not Authorized to Access this Page")
        
        vals = {}
        for i in times:
            vals[i["km"]] = i["open_time"]
                
        return vals

class listCloseOnly1(Resource):
    def get(self):
        global times

        try:
            toke = session["token"]
            if verify_auth_token(toke) != "Success":
                abort(401, error_message = "You Are Not Authorized to Access this Page")
        except:
            abort(401, error_message = "You Are Not Authorized to Access this Page")
        
        
        vals = {}
        for i in times:
            vals[i["km"]] = i["close_time"]
                
        return vals

class listAll2(Resource):
    
    def get(self, spec1):
        global times
        
        try:
            toke = session["token"]
            if verify_auth_token(toke) != "Success":
                abort(401, error_message = "You Are Not Authorized to Access this Page")
        except:
            abort(401, error_message = "You Are Not Authorized to Access this Page")

        top = request.args.get("top")
        
        if spec1 == "csv":
            
            temp_arr = times
            temp_array2 = sorted(temp_arr, key=lambda k: int(k['km']))
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]

            string = ""
            '''
            with open("listall.csv",'w') as csvFile:
                writer = csv.writer(csvFile, delimiter = ",", quotechar = "|", quoting = csv.QUOTE_MINIMAL)
                for i in temp_array2:
                    writer.writerow([i["km"], i["open_time"], i["close_time"]])

            This code doesn't work, but it should. A CSV file is created, but it cannot be displayed to the page. 
            '''
            for i in temp_array2:
                
                string += "km:" + i["km"] + ", " + "open time:" + i["open_time"] + ", " + "close time:" + i["close_time"] + ", "
                    
            return string

        elif spec1 == "json":

            temp_array = times
            temp_array2 = sorted(temp_array, key=lambda k: int(k['km'])) 
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]
                
            vals = {}
            for i in temp_array2:
                vals[i["km"]] = ["open time: " + i["open_time"], "close time: " + i["close_time"]]

            return vals
            

        
class listOpenOnly2(Resource):

    def get(self, spec2):
        global times

        try:
            toke = session["token"]
            if verify_auth_token(toke) != "Success":
                abort(401, error_message = "You Are Not Authorized to Access this Page")
        except:
            abort(401, error_message = "You Are Not Authorized to Access this Page")
        

        top = request.args.get("top")
        
        if spec2 == "csv":

            temp_arr = times
            temp_array2 = sorted(temp_arr, key=lambda k: int(k['km'])) 
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]

            string = ""
            for i in temp_array2:
                string += "km:" + i["km"] + ", " +  "open time:" + i["open_time"] + ", "

            return string
        
        elif spec2 == "json":

            temp_array = times
            temp_array2 = sorted(temp_array, key=lambda k: int(k['km']))
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]
                
            vals = {}
            for i in temp_array2:
                vals[i["km"]] = i["open_time"]
                
            return vals

class listCloseOnly2(Resource):
    def get(self, spec3):
        global times

        try:
            toke = session["token"]
            if verify_auth_token(toke) != "Success":
                abort(401, error_message = "You Are Not Authorized to Access this Page")
        except:
            abort(401, error_message = "You Are Not Authorized to Access this Page")

        top = request.args.get("top")
        if spec3 == "csv":
            
            temp_arr = times
            temp_array2 = sorted(temp_arr, key=lambda k: int(k['km']))
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]
                
                
            string = ""
            for i in temp_array2:
                string += "km:" + i["km"] + ", " +  "close time:" + i["close_time"] + ", "

            return string
        
        elif spec3 == "json":

            temp_array = times
            temp_array2 = sorted(temp_array, key=lambda k: int(k['km']))
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]
                
            vals = {}
            for i in temp_array2:
                vals[i["km"]] = i["close_time"]
                
            return vals

# Create routes
# Another way, without decorators
api.add_resource(listAll1, "/listAll")
api.add_resource(listOpenOnly1, "/listOpenOnly")
api.add_resource(listCloseOnly1, "/listCloseOnly")

api.add_resource(listAll2, "/listAll/<spec1>")
api.add_resource(listOpenOnly2, "/listOpenOnly/<spec2>")
api.add_resource(listCloseOnly2, "/listCloseOnly/<spec3>")


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)
    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=CONFIG.PORT, debug=True)

